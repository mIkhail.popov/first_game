class Bbox{
    constructor(x,y,w,h){
        this.x=x;
        this.y=y;
        this.w=w;
        this.h=h;

        this.max_y=y+h;
        this.max_x=x+w;
    }

    draw(x,y,w,h){
        render.fillStyle="rgb(255,0,0,0.5)";
        render.fillRect(x,y,w,h)
    }
    draw_two(x,y,w,h){
        render.fillStyle="rgb(0,0,255)";
        render.fillRect(x,y,2,2)
    }
}