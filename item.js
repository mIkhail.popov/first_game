
var items = []


class Item{
    constructor(x, y, type, id){
        this.x = x, this.y = y;
        this.w = 45, this.h = 45;
        this.type = type;
        this.id = id
        this.inInvent = false;
        this.crafting = false;
        this.isUse = false;
        this.gravity = 4

        obj.push(this)
        items.push(this)
    }
    step(){

        this.y += this.gravity;
        if(d.y<=this.y+this.h){
            this.gravity=0
            this.y=d.y-this.h
        }

        if(this.crafting){
            this.type = null
        }
        if(mouse.x >= inventar.x && mouse.x <= inventar.x+inventar.w && mouse.y >= inventar.y && mouse.y <= inventar.y+inventar.h){
            null
        }
        if(mouse.x >= this.x && mouse.x <= this.x+this.w && mouse.y >= this.y && mouse.y <= this.y+this.h){
            if(inventar.addItem(this)){
                this.inInvent = true;
                console.log(inventar.inventary.length, "len", inventar.inventary)
            }
            else{null}
        }
        
        if(this.inInvent){
            if(inventar.inventary.length == 1){
                this.x = inventar.x
                this.y = inventar.y

            }
            else if(inventar.inventary.length == 2){
                this.x = inventar.x+50
                this.y = inventar.y
            }
            else if(inventar.inventary.length == 3){
                this.x = inventar.x+100
                this.y = inventar.y
            }
            else if(inventar.inventary.length == 4){
                this.x = inventar.x
                this.y = inventar.y+50
            }
            else if(inventar.inventary.length == 5){
                this.x = inventar.x+50
                this.y = inventar.y+50
            }
            else if(inventar.inventary.length == 6){
                this.x = inventar.x + 100
                this.y = inventar.y+50
            }
            
            else if(inventar.inventary.length == 7){
                this.x = inventar.x
                this.y = inventar.y+100
            }
            
            else if(inventar.inventary.length == 8){
                this.x = inventar.x + 50
                this.y = inventar.y+100
            }
            
            else if(inventar.inventary.length == 9){
                this.x = inventar.x + 100
                this.y = inventar.y+100
            }
        }
        if(this.inInvent == false){
            this.x += xd;
        }
        if(keys.enter){
            console.log("start")
            if(inventar.inventary[useSlot]==undefined){null}
            else if(inventar.inventary[useSlot].type=="stick"){
                console.log("stick")
                render.drawImage(images.stick, player.x+40, player.y, this.w, this.h);
            }
            else if(inventar.inventary[useSlot].type=="rock"){
                console.log("rock")
                render.drawImage(images.rock, player.x+40, player.y, this.w, this.h);
            }
            
            else if(inventar.inventary[useSlot].type=="axe"){
                console.log("axe")
                render.drawImage(images.axe, player.x+40, player.y, this.w, this.h);
            }
        }
    }
    draw(){
        if(this.type=="stick"){
            render.drawImage(images.stick, this.x, this.y, this.w, this.h);
        }
        if(this.type=="rock"){
            render.drawImage(images.rock, this.x, this.y, this.w, this.h);
        }
        
        if(this.type=="axe"){
            render.drawImage(images.axe, this.x, this.y, this.w, this.h);
        }
        if(on1){
            strokeSize(2);
            strokecolor(255, 255, 255);
            rectangle(this.x, this.y, this.w, this.h);    
        }
        else null;
        // else {null}
    }
}