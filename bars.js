//health bar

class HealthBar {
    constructor(x, y, w, h, maxHealth, color) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.maxHealth = maxHealth;
        this.maxWidth = w;
        this.health = maxHealth;
        this.color = color;

        // obj.push(this);
    }

    draw() {
        render.strokeStyle = 'black';
        render.lineWidth = 4;
        render.fillStyle = this.color;
        render.fillRect(this.x, this.y, this.w, this.h);
        render.strokeRect(this.x, this.y, this.maxWidth, this.h);
    }

    step(val) {
        this.health = val;
        this.w = (this.health / this.maxHealth) * this.maxWidth 
    }
}

var health = 100;
var healthBarWidth = 200;
var healthBarHeight = 30;
var x = 10
var y = 10

//food bar

class FoodBar {
    constructor(x, y, w, h, maxFood, color) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.maxFood = maxFood;
        this.maxWidth = w;
        this.food = maxFood;
        this.color = color;

        // obj.push(this);
    }

    draw() {
        render.strokeStyle = 'black';
        render.lineWidth = 4;
        render.fillStyle = this.color;
        render.fillRect(this.x, this.y, this.w, this.h);
        render.strokeRect(this.x, this.y, this.maxWidth, this.h);
    }

    step(val) {
        this.food = val;
        this.w = (this.food / this.maxFood) * this.maxWidth 
    }
}

var food = 100;
var foodBarWidth = 200;
var foodBarHeight = 30;
var x = 10
var y = 25
